package teacherlist;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@Service
public class TeacherInformationService {

    @Autowired
    RestTemplate restTemplate;

    @HystrixCommand(
            fallbackMethod = "getUserTeachersFallback",
            threadPoolKey = "getUserTeachers",
            threadPoolProperties = {
                    @HystrixProperty(name="coreSize", value="100"),
                    @HystrixProperty(name="maximumSize", value="120"),
                    @HystrixProperty(name="maxQueueSize", value="50"),
                    @HystrixProperty(name="allowMaximumSizeToDivergeFromCoreSize", value="true"),
            }
    )
    public UserTeacher getUserTeachers(String userId) {
        return restTemplate.getForObject(
                "http://teacher-info-service/teacher/info/" + userId,
                UserTeacher.class);
    }

    public UserTeacher getUserTeacherFallback(String userId) {
        UserTeacher userTeacher = new UserTeacher();
        List<Teacher> list = new ArrayList<>();
        list.add(new Teacher("-1", "Not available", "Not available"));
        userTeacher.setUserTeachers(list);

        return userTeacher;
    }
}
