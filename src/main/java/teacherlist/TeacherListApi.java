package teacherlist;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/list")
public class TeacherListApi {

    @Autowired
    private TeacherInformationService teacherInformationService;
    @Autowired
    private RatingService ratingService;

    @GetMapping("/{userId}")
    public List<TeacherList> getAllTeachers(
            @PathVariable String userId) {



        // get all teachers by userId (teacher)
        UserTeacher userTeacher = teacherInformationService.getUserTeachers(userId);

        List<TeacherList> teacherListList = new ArrayList<>();
        for (Teacher teacher : userTeacher.getUserTeachers()) {
            Rating teacherRating = ratingService.getTeacherRating(teacher.getId());

            teacherListList.add(new TeacherList(teacher.getFirstname(),
                    teacher.getLastname(), teacherRating.getRating()));
        }

        return teacherListList;
    }


}
