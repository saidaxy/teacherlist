package teacherlist;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TeacherList {

    public TeacherList() {
    }

    public TeacherList(String firstname, String lastname, Integer rating) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.rating = rating;
    }

    private String firstname;
    private String lastname;
    private Integer rating;
}
