package teacherlist;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class RatingService {

    @Autowired
    private RestTemplate restTemplate;

    @HystrixCommand(
            fallbackMethod = "getTeacherRatingFallback",
            threadPoolKey = "getTeacherRating",
            threadPoolProperties = {
                    @HystrixProperty(name="coreSize", value="100"),
                    @HystrixProperty(name="maximumSize", value="120"),
                    @HystrixProperty(name="maxQueueSize", value="50"),
                    @HystrixProperty(name="allowMaximumSizeToDivergeFromCoreSize", value="true"),
            })
    public Rating getTeacherRating(String teacherId) {
        return restTemplate.getForObject(
                "http://teacher-ratings-service/rating/" + teacherId,
                Rating.class);
    }
    public Rating getTeacherRatingFallback(String teacherId) {
        return new Rating(teacherId, 0);
    }
}
