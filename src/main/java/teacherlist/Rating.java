package teacherlist;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Rating {

    public Rating() {
    }

    public Rating(String teacherId, Integer rating) {
        this.teacherId = teacherId;
        this.rating = rating;
    }

    private String teacherId;
    private Integer rating; // 0 to 5
}
